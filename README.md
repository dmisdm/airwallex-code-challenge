# Broccoli & Co - React Web App
## Tech Stack
- Typescript & ES6 (Babel stage-0)
- React
- Redux
- Webpack
### Webpack Build Configuration
Process begins at webpack.config.js which merges files accordingly. The primary one being
webpack/common.js, and others being dynamically included in the array of configs that
webpack-merge consumes. It uses process.env.npm_lifecycle_event to determine which module
to require. Each module in webpack/loaders is automatically appended to the config
using requireDir.

## Development Notes
### Start dev server
`npm start`
#### Redux dev tools chrome extention
This is all good to go. Grab the extension from the chrome store.
### Build
`npm run build`
### Testing
`npm run test` to run all tests (files with pattern /.*spec\.tsx/)
Currently using tape as the testing framework, and webpack as the runner.
See testing folder for self created utilities.

## Requirements/Development Notes
- It is assumed that routing and history is not required since there is only one main view
- No more than one modal at a time needs to be displayed, and hence IModalState does not 
need an array of modals, and ModalController does not need to handle rendering more than one.


