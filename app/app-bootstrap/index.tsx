import Root from 'presentation/RootComponent';
import {ProviderWrapper} from 'logic/redux/store';
import './styleImports'; //Barrel import for any stylesheets

export default ProviderWrapper(Root);
