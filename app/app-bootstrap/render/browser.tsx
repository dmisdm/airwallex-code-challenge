import ReactDOM from 'react-dom';

const defaultContainerName = 'app';

export default {
    render: (component, container = document.getElementById(defaultContainerName)) => {
        ReactDOM.render(component, container);
    }
};
