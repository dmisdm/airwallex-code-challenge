export class AppConfig {
    constructor(public apiBaseUrl: string,
                public brandName: string) {
    }
}

export const appConfigInstance = new AppConfig(
    process.env.API_BASE_URL || 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod',
    'Broccoli & Co.'
);
