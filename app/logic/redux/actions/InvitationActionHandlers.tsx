import AbstractActionHandlers from './classes/AbstractActionHandlers';
import PromiseReduxAction from './classes/PromiseReduxAction';
import IInvitationState from '../../../types/state/IInvitationState';
import InvitationRequestsService from 'logic/services/InvitationRequests';
import SynchronousReduxAction from './classes/SynchronousReduxAction';
import IInvitationHTTPRequest from '../../../types/IInvitationHTTPRequest';

class InvitationActionHandlers extends AbstractActionHandlers<IInvitationState> {

    public sendRequest = new PromiseReduxAction<IInvitationState, IInvitationHTTPRequest, any, string>(
        'sendRequest',
        payload => InvitationRequestsService.sendRequest(payload),
        state => Object.assign({}, state, {requesting: true, errorMessage: null, savedRequest: null}),
        state => Object.assign({}, state, {requesting: false}),
        (state, payload) => Object.assign({}, state, {requesting: false, errorMessage: payload})
    );
    public saveRequest = new SynchronousReduxAction<IInvitationState, any>(
        'saveRequest',
        (state, actionPayload) => {
            if (actionPayload) {
                return Object.assign({}, state, {savedRequest: actionPayload});
            }
            else {
                return state;
            }
        }
    );
    public clearSaved = new SynchronousReduxAction<IInvitationState, {}>(
        'clearSaved',
        state => Object.assign({}, state, {savedRequest: null})
    );

    constructor() {
        super();
    }
}

export default new InvitationActionHandlers();
