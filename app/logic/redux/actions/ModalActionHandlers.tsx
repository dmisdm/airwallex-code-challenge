import AbstractActionHandlers from './classes/AbstractActionHandlers';
import SynchronousReduxAction from './classes/SynchronousReduxAction';
import IModalState from 'types/state/IModalState';
import Element = JSX.Element;

class ModalActionHandlers extends AbstractActionHandlers<IModalState> {
    /**
     * Show a modal given a react element.
     */
    public showModal = new SynchronousReduxAction<IModalState, {element: Element}>(
        'showModal',
        (state, actionPayload) => ({
            current: actionPayload.element
        })
    );
    public hideModal = new SynchronousReduxAction<IModalState, {}>(
        'hideModal',
        state => ({
            current: null
        })
    );
    constructor() {
        super();
    }
}

export default new ModalActionHandlers();
