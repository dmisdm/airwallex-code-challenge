# Redux Action Class Wrappers
The action handler classes here are to be consumed by reducer factories in order to route
actions correctly using this implementation, and conveniently and type-safely create actions throughout the app.

To create a new action handlers class:
- Create a class that implements AbstractActionHandlers
- Add properties of type AbstractReduxActionHandler
    - There are currently two concrete implementations: PromiseReduxAction and SynchronousReduxAction.

## Potential Issues
There is currently nothing stopping a developer from entering duplicate action names. One solution is to generate
action names using a UUID maybe, but you would lose clarity when debugging and using redux dev tools.
At the very least, other classes need to be implemented to check for duplicates, and perhaps namespace them.
