import IReduxActionHandler from './AbstractReduxActionHandler';
import * as asyncSuffixes from './asyncSuffixes';
import {Reducer} from 'redux';

/**
 * Remove the first appConfigInstance of an async suffix that is appended dynamically by redux promise middleware
 */
const trimAsyncSuffixes = (name: string) => {
    let suffixes = Object.keys(asyncSuffixes).map((k) => asyncSuffixes[k]);
    for (let i in suffixes) {
        let suffix = suffixes[i];
        let index = name.indexOf(suffix);
        if (index !== -1) {
            return name.replace(suffix, '');
        }
    }
    return name;
};

/**
 * An abstract class to contain generic functions across action handler classes.
 */
abstract class AbstractActionHandlers<State> {
    /**
     * Resolve a reducer given an action type/name. This will trim any suffixes automatically added by promise
     * middleware (eg _PENDING) and resolve to the action handler of that trimmed value
     * (eg addToCart_PENDING resolves to addToCart) from the current class appConfigInstance.
     * The action object passed into the reducer will still remain the same, therefore the getReducer function
     * can discern the original action, which is required in order to process the different promise lifecycle events.
     * See PromiseReduxAction
     */
    public resolveReducer(name: string): Reducer<State> {
        name = trimAsyncSuffixes(name);
        let actions = Object.getOwnPropertyNames(this).map((k) => this[k]);
        let action = actions.find(a => a.getName() === name);
        if (action) {
            return action.getReducer();
        }
    }

    /**
     * Use this to get a map of all actions for the class, for binding all of them to a component
     */
    public getAllCreators() {
        let output = {};
        Object.getOwnPropertyNames(this).forEach((name) => {
            let action = (this[name] as IReduxActionHandler<any>);
            output[name] = action.create.bind(this[name]);
        });
        return output;
    }
}

export default AbstractActionHandlers;
