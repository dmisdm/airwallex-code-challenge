import {Reducer} from 'redux';
import IReduxAction from '../../../../types/IReduxAction';
abstract class AbstractReduxActionHandler<S> {
    constructor(private name: string) {}

    public abstract getReducer(): Reducer<S>;

    public abstract create(...args): IReduxAction;

    public getName(): string {
        return this.name;
    }
}

export default AbstractReduxActionHandler;
