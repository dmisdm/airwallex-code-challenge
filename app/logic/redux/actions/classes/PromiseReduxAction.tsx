import {Reducer} from 'redux';
import AbstractReduxActionHandler from './AbstractReduxActionHandler';
import IReduxAction from '../../../../types/IReduxAction';
import {PENDING, FULFILLED, REJECTED} from './asyncSuffixes';
import {Promise} from 'when';

/**
 * Asynchronous promise redux action class.
 * This is similar to a Synchronous Redux Action however it requires parameters for pending, fulfilled and rejected
 * events from the promise.
 * This class can produce a redux compliant reducer, but enable a developer to create an action with ease.
 */

type FulfilledHandler<State, ISuccess> = {(state: State, result: ISuccess): State};

class PromiseReduxAction<State, ActionPayload, ISuccess, IFailure> extends AbstractReduxActionHandler<State> {
    constructor(name: string,
                private promiseCreator: (payload: ActionPayload) => Promise<ISuccess>,
                private pendingHandler: (state: State, payload: ActionPayload) => State = state => state,
                private fulfilledHandler: FulfilledHandler<State, ISuccess> = state => state,
                private rejectedHandler: (state: State, result: IFailure) => State = state => state) {
        super(name);
    }

    public create(payload: ActionPayload): IReduxAction {
        return {
            type: this.getName(),
            payload: {
                promise: this.promiseCreator(payload),
                data: payload
            }
        };
    }

    public getReducer(): Reducer<any> {
        const name = this.getName();
        return (state: State, action: {type: string, payload}) => {
            switch (action.type) {
                case `${name}${PENDING}`:
                    return this.pendingHandler(state, action.payload);
                case `${name}${FULFILLED}`:
                    return this.fulfilledHandler(state, action.payload);
                case `${name}${REJECTED}`:
                    return this.rejectedHandler(state, action.payload);
                default:
                    return state;
            }
        };
    }
}

export default PromiseReduxAction;
