import {Reducer} from 'redux';
import AbstractReduxActionHandler from './AbstractReduxActionHandler';
import IReduxAction from 'types/IReduxAction';
/**
 * Synchronous redux action class.
 * This is a generic class that requires State and Action Payload interfaces.
 * This class can produce a redux compliant reducer, but enable a developer to create an action with ease.
 */
class SynchronousReduxAction<S, AP> extends AbstractReduxActionHandler<S> {
    constructor(name: string,
                private reducer: (state: S, actionPayload: AP) => S = state => state) {
        super(name);
    }

    public create(payload?: AP): IReduxAction {
        return {
            type: this.getName(),
            payload
        };
    }

    public getReducer(): Reducer<S> {
        return (state: S, action: {type: string, payload: AP}) => this.reducer(state, action.payload);
    }
}

export default SynchronousReduxAction;
