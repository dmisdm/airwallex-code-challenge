/**
 * Suffixes for action names so that we can capture actions from promise middleware in the redux store
 * @type {string}
 */
export const PENDING = '_PENDING';
export const FULFILLED = '_FULFILLED';
export const REJECTED = '_REJECTED';
