import tape from 'blue-tape';
import {configureStore} from '../store';
import InvitationActions from './InvitationActionHandlers';
import InvitationRequest from '../../../types/forms/InvitationRequestForm';
import {stateActionAssertion} from '../../../testing/assertions';
import isActionAsynchronous from '../../../testing/isActionAsynchronous';

tape('Invitation Redux Actions', t => {

    let invitationRequest = new InvitationRequest();
    invitationRequest.name = 'test';
    invitationRequest.email = 'test';
    let invitationRequestData = invitationRequest.getPlainObject();
    let invitationRequestHTTPData = invitationRequest.getHttpModel();

    t.test('save and clear actions', st => {
        st.plan(2);
        let store = configureStore();
        let action = InvitationActions.saveRequest.create(invitationRequestData);
        let clearAction = InvitationActions.clearSaved.create();
        stateActionAssertion(action, store, st, state => ({
            result: state.invitation.savedRequest === invitationRequestData,
            message: 'saves data to the state'
        }));
        stateActionAssertion(clearAction, store, st, state => ({
            result: !state.invitation.savedRequest,
            message: 'then clears saved request'
        }));
    });

    t.test('sendRequest action', st => {
        st.plan(3);
        let store = configureStore();
        let action = InvitationActions.sendRequest.create(invitationRequestHTTPData);
        let actionResult = stateActionAssertion(action, store, st, state => ({
            result: !!state.invitation.requesting,
            message: 'sets requesting to true'
        }));
        let promise = isActionAsynchronous(actionResult, st);
        let requestingFalseMessage = 'sets requesting to false after its finished';
        if (promise) {
            promise.then(() => {
                st.assert(!store.getState().invitation.requesting, requestingFalseMessage );
            });
        }
        else {
            st.fail(requestingFalseMessage);
        }
    });
});
