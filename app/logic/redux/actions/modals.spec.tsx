import tape from 'tape';
import * as React from 'react';
import ModalActions from './ModalActionHandlers';
import {configureStore} from '../store';
import {stateActionAssertion} from '../../../testing/assertions';

tape('Modal actions', t => {
    t.plan(2);
    let store = configureStore();
    let element = <div>Test Modal</div>;
    let showAction = ModalActions.showModal.create({element});
    let hideAction = ModalActions.hideModal.create();

    stateActionAssertion(showAction, store, t, state => ({
        result: state.modal.current === element,
        message: 'showAction correctly sets the element'
    }));
    stateActionAssertion(hideAction, store, t, state => ({
        result: !state.modal.current,
        message: 'hideAction correctly removes the element'
    }));

});
