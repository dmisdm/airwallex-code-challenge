/**
 * Create a redux reducer given an AbstractActionHandlers class.
 * This is essentially the bridge between redux and my own action handler classes.
 * It will set the default parameter of the reducer wrapper to getDefaultState(), injected from above.
 */
import {Reducer} from 'redux';
import AbstractActionHandlers from '../actions/classes/AbstractActionHandlers';
class ReducerFactory<State> {
    public create(getDefaultState: () => State, actionHandlers: AbstractActionHandlers<State>): Reducer<State> {
        return (state = getDefaultState(), action) => {
            const actionHandler: Reducer<State> = actionHandlers.resolveReducer(action.type);

            if (actionHandler) {
                return actionHandler(state, action);
            }
            else {
                return state;
            }
        };
    }
}

export default new ReducerFactory();
