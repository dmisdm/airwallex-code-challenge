import InvitationActionHandlers from '../actions/InvitationActionHandlers';
import ReducerFactory from './ReducerFactory';
import {getDefaultState} from 'types/state/IInvitationState';
export default ReducerFactory.create(getDefaultState, InvitationActionHandlers);
