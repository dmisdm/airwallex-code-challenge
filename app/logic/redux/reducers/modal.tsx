import ModalActionHandlers from '../actions/ModalActionHandlers';
import ReducerFactory from './ReducerFactory';
import {getDefaultState} from 'types/state/IModalState';
export default ReducerFactory.create(getDefaultState, ModalActionHandlers);
