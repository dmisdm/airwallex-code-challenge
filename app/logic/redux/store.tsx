import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import promiseMiddleware from 'redux-promise-middleware';
import IApplicationState from 'types/state/IApplicationState';
import invitation from './reducers/invitation';
import modal from './reducers/modal';
import {Store} from 'redux';
import * as React from 'react';

const reducer = combineReducers<IApplicationState>({
    invitation,
    modal
});

let devTools = ({} || (window as any)).devToolsExtension;

export const configureStore = () => {
    return createStore(reducer, compose(
        applyMiddleware(
            promiseMiddleware(),
        ),
        devTools ? devTools() : f => f
    )) as Store<IApplicationState>;
};

let store = configureStore();
export default store;

export const ProviderWrapper = Component => props => (
    <Provider store={store}>
        <Component {...props} />
    </Provider>
);
