import {appConfigInstance as appConfig} from 'appConfig';
import mime from 'rest/interceptor/mime';
import pathPrefix from 'rest/interceptor/pathPrefix';
import errorCode from 'rest/interceptor/errorCode';
import defaultRequest from 'rest/interceptor/defaultRequest';
import template from 'rest/interceptor/template';
import {Client, wrap} from 'rest';
export class APIService {
    private client: Client;
    constructor(baseUrl: string) {
        /*
         Define our HTTP Client using a decorator pattern type approach.
         We have delegated all authentication rules/logic and actions to AuthInterceptor
         */
        this.client = wrap(mime)
            .wrap(errorCode)
            .wrap(pathPrefix, {
                prefix: baseUrl
            })
            .wrap(defaultRequest, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .wrap(template);
    }

    public getClient(): Client {
        return this.client;
    }
}
export default new APIService(appConfig.apiBaseUrl);
