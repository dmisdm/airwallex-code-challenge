import APIService from '../API';
import {Promise} from 'when';
import IInvitationHTTPRequest from 'types/IInvitationHTTPRequest';
import {reject} from 'when';
class InvitationRequestsService {
    public sendRequest(request: IInvitationHTTPRequest): Promise<string> {
        return APIService.getClient()({
            path: '/fake-auth',
            method: 'POST',
            entity: request
        })
            .then(result => result.entity)
            .catch(result => reject<string>(result.entity.errorMessage));
    }
}
export default new InvitationRequestsService();
