import * as React from 'react';
import classnames from 'classnames';
const {root, largeLogo} = require('./style.scss');
const BaseLogo = ({url, className = '', large = false}) => (
    <div className={classnames(root, className, large ? largeLogo : '')} style={{backgroundImage: `url('${url}')`}} />
);
export default BaseLogo;
