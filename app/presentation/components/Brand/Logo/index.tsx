import Base from './base';
import * as React from 'react';
const whiteLogo = require('assets/images/logo-white.svg');
const logo = require('assets/images/logo.svg');

export const WhiteLogo = (props) => <Base url={whiteLogo} {...props}/>;
export const Logo = (props) => <Base url={logo} {...props}/>;
