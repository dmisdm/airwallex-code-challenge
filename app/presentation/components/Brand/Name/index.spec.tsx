import tape from 'tape';
import BrandName from '.';
import render from 'testing/shallowRender';
import {appConfigInstance} from 'appConfig';
import * as React from 'react';
tape('Brand name', (t) => {
    let component = render(<BrandName />);
    t.equals(component.props.children, appConfigInstance.brandName, 'rendered text reflects appConfig brand name');
    t.end();
});
