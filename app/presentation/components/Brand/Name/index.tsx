import * as React from 'react';
import {appConfigInstance as appConfig} from 'appConfig';
const BrandName = (props) => (
    <span>{appConfig.brandName}</span>
);
export default BrandName;
