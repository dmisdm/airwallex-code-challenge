import * as React from 'react';
const {animated, large, small, button, disabled} = require('./style.scss');
interface IProps extends React.HTMLProps<any> {
    animated?: boolean;
    large?: boolean;
    small?: boolean;
}
const Button = (props: IProps) => {
    let classNames = [
        button,
        props.animated ? animated : '',
        props.large ? large : '',
        props.small ? small : '',
        props.disabled ? disabled : ''
    ];
    return (
        <button className={classNames.join(' ')} onClick={props.onClick} disabled={props.disabled}>
            {props.children}
        </button>
    );
};

export default Button;
