import * as React from 'react';
import omit from 'lodash/omit';
import {FormFieldError} from '../../Typography/index';
import classnames from 'classnames';
const {root, input, errorsContainer, errorsVisible} = require('./style.scss');
const Input = (props: React.HTMLProps<HTMLInputElement> & {errors?: string}) => (
    <div className={root}>
        <input
            {...omit(props, 'className', 'errors')}
            className={classnames(input, props.className)}
        />
        <div className={classnames(errorsContainer, props.errors ? errorsVisible : '')}>
            {props.errors && <FormFieldError >{props.errors}</FormFieldError>}
        </div>
    </div>

);
export default Input;
