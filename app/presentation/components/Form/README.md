# Form component
To create a form, create a class that extends AbstractFormModel, and implement properties
with class-validator annotations. See RequestFormComponent and InvitationRequestForm as an example.

## Future improvements
- Form fields will need custom field types for when requiring form input that isn't just text.
This could be done by making a FieldType class, which would contain a property of FieldComponent,
which would be a special type of react component that has a common interface for eg getting and setting
values, maybe validation etc.
- At the moment the submit button text is fixed
