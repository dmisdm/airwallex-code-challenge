import * as React from 'react';
import autoBind from 'react-autobind';
import curry from 'lodash/curry';
import Input from './Input';
import Button from '../Button';
import {FormError} from '../Typography';
import {Flex} from 'react-flex';
import AbstractFormModel from '../../../types/forms/AbstractFormModel';
import IFormField from '../../../types/forms/IFormField';
import FieldValidationError from '../../../types/forms/FieldValidationError';
const {errors} = require('./style.scss');
const {opaque, fullWidth} = require('../../styles/util.scss');

interface IFormProps<Model> {
    inProgress: boolean;
    model: Model;
    errorMessage: string;
    onSubmit(model: Model): any;
    onChange(model: Model): any;
}

interface IFormState {
    disabled?: boolean;
    modelErrors?: Map<string, FieldValidationError>;
}

class Form<Model extends AbstractFormModel> extends React.Component<IFormProps<Model>, IFormState> {
    private fields: IFormField[];

    constructor(props: IFormProps<Model>) {
        super(props);
        autoBind(this);
        this.state = {
            disabled: false,
            modelErrors: new Map()
        };
    }

    public handleKeyDown(event: React.KeyboardEvent) {
        if (event.keyCode === 13) {
            this.handleSubmitAttempt();
        }
    }

    public handleFieldChange(name, event) {
        this.props.model[name] = event.target.value;
        this.props.onChange(this.props.model);
    }

    public handleSubmitAttempt() {
        this.validate().then(() => !this.state.modelErrors.size && this.submit());
    }

    public componentWillMount() {
        this.fields = this.props.model.getFormFields();
    }

    public componentWillReceiveProps(props) {
        this.state.disabled = props.inProgress;
        this.fields = props.model.getFormFields();
    }

    public validate() {
        return this.props.model.validate()
            .then(
                errs => this.setState(Object.assign({}, this.state, {modelErrors: errs}))
            );
    }

    public submit() {
        this.props.onSubmit(this.props.model);
    }

    public render() {
        const {disabled, modelErrors} = this.state;
        return (
            <div className={disabled ? opaque : ''} onKeyDown={this.handleKeyDown}>
                {this.fields.map(field => (
                    <Flex className={fullWidth} key={field.name} column alignItems="center">

                        <Input
                            disabled={disabled}
                            name={field.name}
                            label={field.label}
                            placeholder={field.placeholder}
                            onChange={curry(this.handleFieldChange)(field.name)}
                            defaultValue={field.value}
                            errors={modelErrors.get(field.name) && modelErrors.get(field.name).getReason()}
                        />

                    </Flex>
                ))}
                <Button
                    onClick={this.handleSubmitAttempt}
                    small
                    disabled={disabled}
                >
                    {this.props.inProgress ? 'Loading, please wait...' : 'Send'}
                </Button>
                <div className={errors}>
                    {this.props.errorMessage && <FormError>{this.props.errorMessage}</FormError>}
                </div>
            </div>
        );
    }

}

export default Form;
