import * as React from 'react';
const base = (el, props) => {
    return (
        React.createElement(el, props, props.children)
    );
};

export const Inline = (props) => base('span', props);
export const Block = (props) => base('div', props);
