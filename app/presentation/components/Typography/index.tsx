import {Inline, Block} from './Base';
import * as React from 'react';
import classnames from 'classnames';
const {
    display4,
    display3,
    display2,
    display1,
    headline,
    title,
    subhead,
    body,
    caption,
    menu,
    button,
    brand,
    formError,
    formFieldError
} = require('presentation/styles/typography.scss');
export const Body = (props) => <Block {...props} className={classnames(body, props.className)} />;
export const Title = (props) => <Block {...props} className={classnames(title, props.className)} />;
export const Display4 = (props) => <Block {...props} className={classnames(display4, props.className)} />;
export const Display3 = (props) => <Block {...props} className={classnames(display3, props.className)} />;
export const Display2 = (props) => <Block {...props} className={classnames(display2, props.className)} />;
export const Display1 = (props) => <Block {...props} className={classnames(display1, props.className)} />;
export const Headline = (props) => <Block {...props} className={classnames(headline, props.className)} />;
export const Subhead = (props) => <Inline {...props} className={classnames(subhead, props.className)} />;
export const Caption = (props) => <Inline {...props} className={classnames(caption, props.className)} />;
export const Menu = (props) => <Inline {...props} className={classnames(menu, props.className)} />;
export const Button = (props) => <Inline {...props} className={classnames(button, props.className)} />;
export const Brand = (props) => <Inline {...props} className={classnames(brand, props.className)} />;
export const FormFieldError = (props) => <Inline {...props} className={classnames(formFieldError, props.className)} />;
export const FormError = (props) => <Inline {...props} className={classnames(formError, props.className)} />;
