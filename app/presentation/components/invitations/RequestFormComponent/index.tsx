import * as React from 'react';
import {connect} from 'react-redux';
import InvitationActions from 'logic/redux/actions/InvitationActionHandlers';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';
import SuccessfulRequestModal from '../../modals/SuccessfulRequest';
import Form from '../../Form';
import InvitationRequest from '../../../../types/forms/InvitationRequestForm';

const propsStateMap = state => ({
    inProgress: state.invitation.requesting,
    errorMessage: state.invitation.errorMessage,
    model: new InvitationRequest(state.invitation.savedRequest)
});

const propsActionsMap = (dispatch, {onChange = e => e}) => ({
    onSubmit: (model: InvitationRequest) => {
        dispatch(InvitationActions.sendRequest.create(model.getHttpModel()))
            .then(
                () => {
                    dispatch(ModalActions.showModal.create({element: <SuccessfulRequestModal />}));
                    dispatch(InvitationActions.clearSaved.create());
                }
            );
    },
    onChange: (model: InvitationRequest) => onChange(model.getPlainObject())
});

export default connect(propsStateMap, propsActionsMap)(Form);
