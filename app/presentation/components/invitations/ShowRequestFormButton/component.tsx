import * as React from 'react';
import Button from '../../Button';

const ShowRequestFormButton = (props: {onClick?: () => any}) => (
    <div onClick={props.onClick}>
        <Button large animated >Request an invite</Button>
    </div>
);

export default ShowRequestFormButton;
