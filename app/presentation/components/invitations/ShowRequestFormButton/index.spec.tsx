import Component from '.';
import tape from 'blue-tape';
import render from 'testing/shallowRender';
import * as React from 'react';
import {configureStore} from 'logic/redux/store';
import InvitationRequest from '../../modals/InvitationRequest';
import {stateAssertion} from '../../../../testing/assertions';

tape('ShowRequestForm', t => {

    t.plan(1);
    let store = configureStore();
    let component = render(React.createElement(Component), {store});
    stateAssertion(store, t, state => ({
            result: state.modal.current.type === InvitationRequest,
            message: 'dispatches a showModal action with the InvitationRequest component'
        }),
        component.props.onClick);
});
