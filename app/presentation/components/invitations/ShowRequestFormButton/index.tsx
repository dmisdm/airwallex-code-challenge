import component from './component';
import {connect} from 'react-redux';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';
import InvitationRequestModal from '../../modals/InvitationRequest';
import * as React from 'react';
let propsActionsMap = dispatch => ({
    onClick: () => {
        dispatch(ModalActions.showModal.create({element: <InvitationRequestModal/>}));
    }
});
export default connect(null, propsActionsMap)(component);
