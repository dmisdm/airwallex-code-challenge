import * as React from 'react';
import {Subhead} from '../../Typography/index';
import {Close, Divider} from 'rebass';
import {Flex, Item} from 'react-flex';
const {root} = require('./style.scss');

const BasicModal = (props: {onClose?; title; children?}) => (
    <Flex column alignItems="stretch" justifyContent="center" className={root}>
        <Flex flex={4} row justifyContent="end">
            <Close onClick={props.onClose}/>
        </Flex>
        <Flex flex={6} column justifyContent="center">
            <Subhead>{props.title}</Subhead>
            <Divider
                width={30}
            />
        </Flex>
        <Item flex={20}>
            {props.children}
        </Item>

    </Flex>
);

export default BasicModal;
