import * as React from 'react';
import RequestForm from '../../invitations/RequestFormComponent';
import BasicModal from '../BasicModal';
import autoBind from 'react-autobind';

class InvitationRequestModal extends React.Component<{onClose}, {}> {
    private currentData;
    constructor() {
        super();
        autoBind(this);
    }
    public handleChange(data) {
        this.currentData = data;
    }
    public handleOnClose() {
        this.props.onClose(this.currentData);
    }
    public render() {
        return (
            <BasicModal title="Request an Invite" onClose={this.handleOnClose}>
                <RequestForm onChange={this.handleChange} />
            </BasicModal>
        );
    };
}

export default InvitationRequestModal;
