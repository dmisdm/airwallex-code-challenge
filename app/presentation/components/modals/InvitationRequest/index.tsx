import {connect} from 'react-redux';
import component from './component';
import InvitationRequestActions from 'logic/redux/actions/InvitationActionHandlers';

const actionsMap = (dispatch) => ({
    onClose: (data) => dispatch(InvitationRequestActions.saveRequest.create(data))
});

export default connect(null, actionsMap)(component);
