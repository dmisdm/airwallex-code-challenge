import tape from 'tape';
import ModalWrapper from '.';
import * as React from 'react';
import render from 'testing/shallowRender';
import {configureStore} from 'logic/redux/store';
import {stateAssertion} from '../../../../testing/assertions';

tape('Modal Wrapper', t => {
    t.plan(2);
    let store = configureStore();
    let testComponent = (props) => React.createElement('div' );
    let Wrapped = ModalWrapper(testComponent);
    let renderedWrapper = render(<Wrapped />, {store});
    let renderedComponent = render(renderedWrapper.props.children, {store});
    t.assert(renderedComponent.type === testComponent, 'renders its parameter which is a component');
    stateAssertion(store, t, state => ({
        result: !state.modal.current,
        message: 'onClose property/function removes current modal from state'
    }), () => {
        renderedComponent.props.onClose();
    });

});
