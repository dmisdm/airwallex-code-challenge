import * as React from 'react';
import {ModalDialog} from 'react-modal-dialog';
import {connect} from 'react-redux';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';
import Element = JSX.Element;
const {root} = require('./style.scss');

type PropsType<T> = T & {onClose?(): any};
type ComponentType<T> = (props: PropsType<T>) => Element;

/**
 * Modal Wrapper HOC to populate the onClose property with the correct redux action, and to wrap with the default modal
 * container (places in the center of the screen, adds shadow and transitions etc. see react-modal-dialog.)
 */
function ModalWrapper<T>(Component: ComponentType<T & {onClose(): any}>) {
    return (props: PropsType<T>) => {
        const propsActionsMap = (dispatch, ownProps: {onClose?(): any}) => ({
                onClose: () => {
                    dispatch(ModalActions.hideModal.create());
                    if (ownProps.onClose) {
                        ownProps.onClose();
                    }
                }
            });
        const ConnectedComponent = connect(null, propsActionsMap)(Component);
        return (
            <ModalDialog className={root}>
                <ConnectedComponent {...props}/>
            </ModalDialog>
        );
    };
}
export default ModalWrapper;
