# Modal system
- The currently displayed modal is stored in the modal state as an element.
(see IModalState and ModalActionHandlers)
- To display a modal, dispatch the showModal action with the element as the payload.
The element passed must be responsible for positioning and styling itself, however i have created
a HOC that wraps an element within a ModalDialog (from react-modal-dialog package), that will
populate the onClose property of its component parameter.
- The ModalController component in the Shell view is the container that
will render whatever element is in the state.
- Only one modal can be displayed at a time for now, until more needed (not hard to implement).
