import * as React from 'react';
import {Flex} from 'react-flex';
import {Body} from '../../Typography';
import BrandName from '../../Brand/Name';
import Button from '../../Button';
import {Logo} from '../../Brand/Logo/index';
const {root} = require('./style.scss');
const SuccessfulRequest = ({onClose = e => e}) => (
    <Flex column alignItems="center" justifyContent="space-around" className={root}>
        <Logo large />
        <Flex flex={1} column justifyContent="space-around">
            <Body>You will be one of the first to experience <BrandName/> when we launch.</Body>

            <Button small onClick={onClose}>
                OK
            </Button>
        </Flex>

    </Flex>
);
export default SuccessfulRequest;
