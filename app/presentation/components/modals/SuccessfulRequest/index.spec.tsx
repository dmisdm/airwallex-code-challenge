import tape from 'tape';
import SuccessfulRequest from '.';
import {stateAssertion} from 'testing/assertions';
import {configureStore} from 'logic/redux/store';
import render from 'testing/shallowRender';
import * as React from 'react';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';
tape('Successful Request Modal', t => {
    t.plan(1);
    const store = configureStore();
    store.dispatch(ModalActions.showModal.create({element: <div/>}));
    const instance = render(<SuccessfulRequest />, {store});
    const childInstance = render(instance.props.children, {store});

    stateAssertion(store, t, state => ({
        result: !state.modal.current,
        message: 'onClose property works'
    }),
    childInstance.props.onClose);
});
