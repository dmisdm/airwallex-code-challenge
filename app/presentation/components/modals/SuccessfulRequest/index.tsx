import * as React from 'react';
import BasicModal from '../BasicModal';
import Component from './component';
import {connect} from 'react-redux';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';

const actionsMap = dispatch => ({
    onClose: () => dispatch(ModalActions.hideModal.create())
});

const ConnectedComponent = connect(null, actionsMap)(Component);
const SuccessfulRequest = () => (
    <BasicModal title="All done!">
        <ConnectedComponent />
    </BasicModal>
);
export default SuccessfulRequest;
