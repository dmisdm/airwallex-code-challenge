import * as React from 'react';
import {Display1, Subhead} from 'presentation/components/Typography/index';

const LandingText = (props) => (
    <div {...props}>
        <Display1>A better way to enjoy every day.</Display1>
        <Subhead>Be the first to know when we launch.</Subhead>
    </div>

);

export default LandingText;
