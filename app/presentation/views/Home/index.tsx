import * as React from 'react';
import {Flex} from 'react-flex';
import LandingText from './LandingText';
import ShowRequestFormButton from '../../components/invitations/ShowRequestFormButton';
const {root, landingTextWrapper, wrapper} = require('./style.scss');
const Home = () => (
    <Flex className={root} column justifyContent="center">
        <Flex  className={wrapper} column>
            <div className={landingTextWrapper}>
                <LandingText />
            </div>
            <ShowRequestFormButton/>
        </Flex>
    </Flex>
);

export default Home;
