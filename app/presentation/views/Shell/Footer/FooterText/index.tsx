import * as React from 'react';
import {Caption} from 'presentation/components/Typography/index';
import BrandName from 'presentation/components/Brand/Name';
const {root} = require('./style.scss');
const FooterText = () => (
    <div className={root}>
        <div>
            <Caption>Made with &hearts; in Melbourne</Caption>
        </div>
        <div>
            <Caption>&copy; <BrandName/> All rights reserved.</Caption>
        </div>
    </div>
);

export default FooterText;
