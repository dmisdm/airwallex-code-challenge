import * as React from 'react';
import FooterText from './FooterText';
import {Flex} from 'react-flex';
const {root} = require('./style.scss');
const Footer = () => (
    <Flex className={root} row justifyContent="center">
        <FooterText />
    </Flex>
);
export default Footer;
