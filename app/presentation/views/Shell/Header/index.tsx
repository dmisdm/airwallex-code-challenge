import * as React from 'react';
import {Brand} from '../../../components/Typography/index';
import BrandName from 'presentation/components/Brand/Name';
import {Flex} from 'react-flex';
import {WhiteLogo} from '../../../components/Brand/Logo/index';
let {root, logoContainer} = require('./style.scss');

const Header = () => (
    <Flex className={root} wrap={false} row alignItems="center">
        <WhiteLogo className={logoContainer} />
        <Brand>
            <BrandName/>
        </Brand>
    </Flex>
);
export default Header;
