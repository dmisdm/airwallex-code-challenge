import * as React from 'react';
import {ModalContainer} from 'react-modal-dialog';

const ModalController = ({currentModal}) => {
    return (
        currentModal &&
        <ModalContainer backgroundColor="rgba(0,0,0,0.6)">
            {currentModal}
        </ModalContainer>
    );
};

export default ModalController;
