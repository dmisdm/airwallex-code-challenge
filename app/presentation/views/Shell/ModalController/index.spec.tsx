import tape from 'blue-tape';
import render from 'testing/shallowRender';
import ModalController from '.';
import {configureStore} from 'logic/redux/store';
import ModalActions from 'logic/redux/actions/ModalActionHandlers';
import * as React from 'react';
tape('Modal Controller', t => {
    t.plan(1);
    const store = configureStore();
    const testModal = <div>TestModal</div>;
    const showModalAction = ModalActions.showModal.create({element: testModal});
    store.dispatch(showModalAction);
    const modalControllerInstance = render(render(<ModalController />, {store}));
    const childIsTestModal = modalControllerInstance.props.children === testModal;
    t.assert(childIsTestModal, 'shows correct modal after showModal action has been dispatched');
});
