import {connect} from 'react-redux';
import ModalController from './component';
const statePropsMap = ({modal}) => ({
    currentModal: modal.current
});
export default connect(statePropsMap)(ModalController);
