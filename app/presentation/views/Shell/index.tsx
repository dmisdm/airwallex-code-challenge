import * as React from 'react';
import Home from '../Home';
import Header from './Header';
import Footer from './Footer';
import {Flex, Item} from 'react-flex';
import ModalController from './ModalController';
const {root, contentContainer, viewContainer} = require('./style.scss');
const Shell = (props, context) => (
    <div className={root} >
        <Flex column className={contentContainer} wrap={false}>
            <Header />
            <Item className={viewContainer}>
                <Home />
            </Item>
            <Footer />
        </Flex>
        <ModalController/>
    </div>
);

export default Shell;
