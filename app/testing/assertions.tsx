import {Store} from 'redux';
import {Test} from 'tape';
import {Action} from 'redux';

const storeBinder = (store, t, callback) => {
    const unsubscribe = store.subscribe(() => {
        const result = callback(store.getState());
        t.assert(result.result, result.message);
        unsubscribe();
    });
};

/**
 *  Given an action, store and a tape tester, bind a self unsubscribing subscriber to the store and execute a callback function
 *  within the subscriber. The callback function should return a result that will determine whether the test should pass,
 *  and a message to describe the test. Optionally pass in an executor that will be executed after the action has been
 *  dispatched.
 */
export
function stateActionAssertion<T>(action: Action, store: Store<T>, t: Test, callback: (state: T) => {result: boolean; message: string}, executor?: () => any) {
    storeBinder(store, t, callback);
    const result = store.dispatch(action);
    if (executor) {
        executor();
    }
    return result;
}

/**
 *  Similar to stateActionAssertion however without dispatching an action. It is expected that the client (function executor)
 *  will either pass it within the executor parameter, or expect an action to be dispatched later.
 */
export function stateAssertion<T>(store: Store<T>, t: Test, callback: (state: T) => {result: boolean; message: string}, executor?: () => any) {
    storeBinder(store, t, callback);
    if (executor) {
        executor();
    }
}
