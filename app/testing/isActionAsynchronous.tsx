import {Test} from 'tape';
export default function isActionAsynchronous(action: any, t: Test) {
    const message = 'is asynchronous';
    let pass = t.pass.bind(null, 'is asynchronous');
    if (action.then) {
        return action.then(pass, pass);
    }
    else {
        t.fail(message);
    }
}
