import TestUtils from 'react-addons-test-utils';
const ShallowRender = (element, context?) => {
    const renderer = TestUtils.createRenderer();
    renderer.render(element, context);
    return renderer.getRenderOutput();
};

export default ShallowRender;
