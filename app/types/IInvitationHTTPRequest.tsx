interface IInvitationHTTPRequest {
    name: string;
    email: string;
}
export default IInvitationHTTPRequest;
