interface IInvitationRequest {
    name: string;
    email: string;
    confirmEmail: string;
}
export default IInvitationRequest;
