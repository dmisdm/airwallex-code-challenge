interface IInvitationRequestFailure {
    errorMessage: string;
}
export default IInvitationRequestFailure;
