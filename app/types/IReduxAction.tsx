
interface IReduxAction {
    type: string;
    payload: any;
}

export default IReduxAction;
