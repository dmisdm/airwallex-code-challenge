declare module 'react-modal-dialog' {
    import React = __React;
    interface IModalContainerProps extends React.HTMLProps<any> {
        onClose?: () => any;
    }

    interface IModalDialogProps extends React.HTMLProps<any> {
        onClose?: () => any;
    }

    export class ModalContainer extends React.Component<IModalContainerProps, any> {}
    export class ModalDialog extends React.Component<IModalDialogProps, any> {}
}
