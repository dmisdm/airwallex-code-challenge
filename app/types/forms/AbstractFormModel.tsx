import {validate, ValidationError} from 'class-validator';
import {metaKey} from './Decorators';
import IFormField from './IFormField';
import map from 'lodash/map';
import FieldValidationError from './FieldValidationError';

class AbstractFormModel {

    /**
     * Initialize the class using a plain object
     */
    constructor(initialData?) {
        if (initialData) {
            Object.keys(initialData).forEach(key => this[key] = initialData[key]);
        }
    }
    public validate(): Promise<Map<string, ValidationError>> {
        return validate(this)
            .then(errors => {
                let output = new Map();
                errors.forEach(error => output.set(error.property, new FieldValidationError(error)));
                return output;
            });
    }

    /**
     * Get fields which have been annotated with @FormField
     */
    public getFormFields(): IFormField[] {
        return map(this[metaKey], v => v as IFormField);
    }
    public getPlainObject() {
        let output = {};
        for (let i in this) {
            if (i !== metaKey) {
                output[i] = this[i];
            }
        }
        return output;
    }
}

export default AbstractFormModel;
