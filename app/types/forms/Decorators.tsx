import curry from 'lodash/curry';
import {IsEmail, IsNotEmpty, registerDecorator, ValidationOptions, ValidationArguments} from 'class-validator';
export const metaKey = 'formMeta';

type formFieldType = 'text';

export const FormField = (type: formFieldType, label: string, placeholder: string) => (
    (target: any, key: string) => {
        let _value = target[key];
        const setMetadata = (value) => target[metaKey] = Object.assign({}, target[metaKey], {
            [key]: {
                label,
                placeholder,
                type,
                value,
                name: key
            }
        });
        setMetadata(_value);
        const setter = val => {
            _value = val;
            setMetadata(val);
        };
        if (delete target[key]) {
            Object.defineProperty(target, key, {
                set: setter,
                get: () => _value,
                enumerable: true,
                configurable: true
            });
        }
    }
);

export function MatchesProperty(property: string, validationOptions?: ValidationOptions) {
    return (object: Object, propertyName: string) => {
        registerDecorator({
            name: 'matchesProperty',
            target: object.constructor,
            propertyName,
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return value === args.object[property];
                },
                defaultMessage() {
                    return `Does not match ${property} field`;
                }
            }
        });
    };
}

export const Email = curry(IsEmail, 2)({})({
    message: 'Invalid email'
});
export const Required = curry(IsNotEmpty)({
    message: 'Required'
});
