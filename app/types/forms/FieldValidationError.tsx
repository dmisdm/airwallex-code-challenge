import {ValidationError } from 'class-validator';
import map from 'lodash/map';
export default class FieldValidationError extends ValidationError {
    constructor(validationError: ValidationError) {
        super();
        this.constraints = validationError.constraints;
        this.value = validationError.value;
        this.children = validationError.children;
        this.property = validationError.property;
        this.target = validationError.target;
    }
    public getReason(): string {
        return map(this.constraints).join(', ');
    }
}
