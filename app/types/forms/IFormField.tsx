interface IFormField {
    label: string;
    name: string;
    type: string;
    placeholder: string;
    value: string;
}
export default IFormField;
