import IInvitationHTTPRequest from '../IInvitationHTTPRequest';
import AbstractFormModel from './AbstractFormModel';
import {FormField, Required, Email, MatchesProperty} from './Decorators';

export default class InvitationRequest extends AbstractFormModel {

    @Required
    @FormField('text', '', 'Name')
    public name: string;
    @Required
    @Email
    @FormField('text', '', 'Email')
    public email: string;
    @Required
    @Email
    @MatchesProperty('email', {
        message: 'Emails do not match'
    })
    @FormField('text', '', 'Confirm Email')
    public confirmEmail: string;

    public getHttpModel(): IInvitationHTTPRequest {
        return {
            name: this.name,
            email: this.email
        };
    }
}
