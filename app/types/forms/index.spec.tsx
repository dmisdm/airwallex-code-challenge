import tape from 'tape';
import AbstractFormModel from './AbstractFormModel';
import {Email, Required, FormField, MatchesProperty} from './Decorators';
tape('Forms', t => {
    const placeholder = 'placeholder test';
    const label = 'label test';
    const type: 'text' = 'text';
    class FormTest extends AbstractFormModel {
        @Email
        @FormField(type, label, placeholder)
        public email: string;
        @Required
        @FormField(type, label, placeholder)
        public other: string;
        @FormField(type, label, placeholder)
        public notValidated: string;
        @Email
        @MatchesProperty('email')
        public confirmEmail: string;
    }
    let formTest = new FormTest();
    formTest.email = 'not-an-email';
    formTest.confirmEmail = 'test@test.com';

    t.test('Decorator validations', sub => {
        formTest.validate().then(errors => {
            sub.assert(errors.get('email'), 'email validator works');
            sub.assert(errors.get('confirmEmail'), 'property matching validator works');
            sub.assert(errors.get('other'), 'is-required validator works');
            sub.end();
        });
    });
    t.test('AbstractFormModel', sub => {
        let fields = formTest.getFormFields();
        sub.plan(2);
        sub.assert(fields.filter(field => {
                return field.placeholder === placeholder
                    && field.label === label
                    && field.name
                    && field.type === type;
            }).length === 3,
            'can produce valid form fields for usage in react components');
        formTest.validate().then(errs => sub.assert(errs.size === 3, 'can produce correct number of validation errors'));
    });
});
