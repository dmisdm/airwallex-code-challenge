import IInvitationState from './IInvitationState';
import IModalState from './IModalState';
interface IApplicationState {
    invitation: IInvitationState;
    modal: IModalState;
}

export default IApplicationState;
