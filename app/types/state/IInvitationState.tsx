import InvitationRequest from '../forms/InvitationRequestForm';
interface IInvitationState {
    requesting: boolean;
    errorMessage?: string;
    savedRequest?: InvitationRequest;
}

export const getDefaultState = (): IInvitationState => ({
    requesting: false
});

export default IInvitationState;
