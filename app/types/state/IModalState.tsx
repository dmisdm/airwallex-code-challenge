import Element = JSX.Element;
interface IModalState {
    current: Element;
}
export const getDefaultState = (): IModalState => ({
    current: null
});
export default IModalState;
