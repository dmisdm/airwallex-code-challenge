/**
 * This is the root webpack config file that will dynamically choose and merge configs in the webpack folder
 * wrt npm_lifecycle_event.
 * Eg npm run start will merge webpack/common.js + webpack/start.js
 *
 * We can easily modify this if we want to merge more than two eg for different environments.
 */
const merge = require('webpack-merge');
const validator = require('webpack-validator');
const config = require('./webpack/config');
const requireDir = require('require-dir');
const _ = require('lodash');


//Get each loader in the loader directory, and execute them passing in config variables. Map this to an array of returned objects
function getLoaders() {
   return _.map(requireDir('./webpack/loaders/'),function(l) {return l(config);});

}

var partitions = [];


partitions.push(config.webpackConfigs.common(config));
//Add each loader found to the list of config partitions
partitions = partitions.concat(getLoaders());
partitions.push(config.webpackConfigs.lifecycle(config) || {});


module.exports = validator(merge.apply(this,partitions));
