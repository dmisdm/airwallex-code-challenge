/**
 *
 * Common configuration export that is used across all build configs
 */
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
module.exports = function (config) {
    return {
        devtool: 'source-map',
        entry: ['babel-polyfill', config.app],
        output: {
            path: config.build,
            filename: '[name].js'
        },
        plugins: [
            new FaviconsWebpackPlugin(config.logoLocation),
            new HtmlWebpackPlugin({
                template: config.template
            })
        ],
        resolve: {
            extensions: ['', '.ts', '.tsx', '.js', '.jsx'],
            root: [
                config.app
            ]

        }
    };

};
