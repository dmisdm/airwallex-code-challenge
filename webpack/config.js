/**
 * Configuration to be used in modules relating to building with webpack.
 * Use process.env.npm_lifecycle_event to determine which modules to require, and hence how webpack runs
 */
const defaultTarget = "build";
const TARGET = process.env.npm_lifecycle_event || defaultTarget;
const ENV = process.env.NODE_ENV || 'development';
const path = require('path');

var conf = {};
conf.app = path.join(__dirname, '..', 'app');
conf.build = path.join(__dirname, '..', 'build');
conf.template = path.join(conf.app, 'index.html');
conf.style = path.join(conf.app, 'style');
conf.assets = path.join(conf.app, 'assets');
conf.testFilesGlob = path.join(conf.app, '**/*.spec.tsx');
conf.env = ENV;
conf.isDev = function () {
    return !TARGET.toString().startsWith('build');
};
conf.isTest = function () {
    return TARGET.toString().startsWith('test');
};
conf.webpackConfigs = {
    common: require('./common'),
    lifecycle: require('./' + TARGET)
};
conf.logoLocation = path.resolve(conf.app, 'assets/images/logo.svg');

module.exports = conf;
