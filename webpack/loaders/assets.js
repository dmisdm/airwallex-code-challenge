/**
 * Created by Daniel on 6/7/2016.
 */
module.exports = function(config) {
  "use strict";
  return  {
    module: {
      loaders: [
        {
          test:/\.(png|jpg|svg)$/,
          loader:'url-loader?limit=8192',
          include:config.app
        },
        {
          test:/\.html$/,
          loader:'html'
        }
      ]
    }
  }
};
