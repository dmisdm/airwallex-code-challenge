module.exports = function(config) {
    return {
        module: {
            loaders: [
                {
                    test:/\.json$/,
                    include:config.app,
                    loader:'json-loader'
                }
            ]
        }
    }

}