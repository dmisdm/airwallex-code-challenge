/**
 * Created by Daniel on 6/7/2016.
 */
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const cssLoader = 'css?modules&importLoaders=1&localIdentName=[name]_[local]__[hash:base64:5]';

module.exports = function (config) {
    "use strict";

    const sassLoaderConfig = {
        includePaths: [
            config.app
        ],
        sourceMap: true
    };

    return {
        module: {
            loaders: [
                {
                    test: /\.(css|scss)$/,
                    loader: ExtractTextPlugin.extract('style?sourceMap', cssLoader + '!sass?' + JSON.stringify(sassLoaderConfig) + '!postcss'),
                    include: config.app
                },
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract('style', 'css'),
                    include: path.resolve('node_modules')
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin('style.css')
        ],
        postcss: function () {
            return [
                require('autoprefixer')
            ];
        }

    }
};
