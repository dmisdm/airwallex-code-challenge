/**
 * Created by Daniel on 6/7/2016.
 */
module.exports = function (config) {
    "use strict";
    const test = /\.(tsx|ts)$/;
    return {
        module: {
            preLoaders: [
                {
                    test: test,
                    loader: 'tslint',
                    include: config.app
                }
            ],
            loaders: [
                {
                    test: test,
                    loaders: ['babel', 'awesome-typescript-loader'],
                    include: config.app
                }
            ]
        },
        tslint: {
            formatter: 'msbuild'
        }
    }
};
