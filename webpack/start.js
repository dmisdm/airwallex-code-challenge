/**
 * Created by Daniel on 6/7/2016.
 */
const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');
const port = process.env.PORT || 8888;
const path = require('path');

module.exports = function (config) {
    return {
        entry: [
            config.app
        ],
        devServer: {
            historyApiFallback: true,
            contentBase: config.app,
            hot: true,
            inline: true,
            stats: 'errors-only',
            open: true,
            port: port
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin({
                multiStep: true
            }),
            new WebpackNotifierPlugin({alwaysNotify: true})
        ]
    }

};
