/**
 * Created by Daniel on 6/7/2016.
 */
var path = require('path');
var webpack = require('webpack');
var TapWebpackPlugin = require('tap-webpack-plugin');
module.exports = function (config) {
    return {
        entry: path.resolve('webpack.test.js'),
        target: 'node',
        output: {
            path: config.build,
            filename: 'test.js'
        },
        plugins: [
            new TapWebpackPlugin({reporter: 'tap-spec'}),
            new webpack.IgnorePlugin(/vertx/)
        ],
        node: {
            vertx: false
        }
    };
};
